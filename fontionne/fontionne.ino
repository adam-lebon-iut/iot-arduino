
#include <LiquidCrystal.h>

//Déclaration LCD
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Gestion potentiomètre, led
const int potPin = A0, ledPin = 13, tempPin = A4;
int valPot = 0, valTemp = 0;
//Initialisation des variables nécéssaires au calcul
float temperatureActuelle, temperatureDemandee, voltage;

void setup() {
  // Déclaration LCD
  lcd.begin(16, 2);
    
  // On déclare les capteurs et la led
  pinMode(potPin,INPUT);
  pinMode(tempPin,INPUT);
  pinMode(ledPin,OUTPUT);
}

void loop() {
  lcd.clear();
  
  //On récupère les valeurs des 2 capteurs
  valPot = analogRead(potPin);
  valTemp = analogRead(tempPin);
  
  temperatureDemandee = valPot / 25.575;
  voltage = (valTemp/1024.0) *  5.0;
  temperatureActuelle = (voltage - .5) * 100;
  
  if (temperatureActuelle > temperatureDemandee) {
    digitalWrite(ledPin,HIGH);
  } else {
     digitalWrite(ledPin,LOW);
  }
   
  //On affiche les valeurs calculées plutot
  lcd.setCursor(0, 0);
  lcd.print("demandee ");
  lcd.print(temperatureDemandee);
  lcd.setCursor(0,1);
  lcd.print("reelle ");
  lcd.print(temperatureActuelle);
  
  delay(500);
}
