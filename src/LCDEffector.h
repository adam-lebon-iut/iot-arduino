#ifndef LCD_EFFECTOR_H
#define LCD_EFFECTOR_H

#include <Arduino.h>
#include <LiquidCrystal.h>
#include <Wire.h>

class LCDEffector {

    private:
        LiquidCrystal *m_lcd;

    public:
        LCDEffector(int rs, int en, int d4, int d5, int d6, int d7);
        void init();
        void print(String message[], int length);
};

#endif
