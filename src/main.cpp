#include <Arduino.h>
#include <RotaryAngleSensor.h>
#include <TemperatureSensor.h>
#include <TemperatureRegulator.h>
#include <LCDEffector.h>
#include <LiquidCrystal.h>

// Configuration des pins de l'écran LCD
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;


LCDEffector lcd(rs, en, d4, d5, d6, d7);
TemperatureSensor tempSensor(A4);
RotaryAngleSensor rotarySensor(A2);

TemperatureRegulator regulator(
    tempSensor,
    rotarySensor,
    lcd
);

void setup()
{
    /**
     * Initialisation des sensors/effectors
     * pinMode, i2c, ...
     **/
    tempSensor.init();
    rotarySensor.init();
}

void loop() {
    /**
     * On demande au régulator de vérifier les valeurs des sensors
     * Si les valeurs ont changés, l'affichage et les consignes
     * sont mis à jour
     **/
    regulator.doObservations();
    delay(100);
}
