#ifndef ROTARY_ANGLE_SENSOR_H
#define ROTARY_ANGLE_SENSOR_H

#include<Sensor.h>

class RotaryAngleSensor: public Sensor {

    using Sensor::Sensor;

    public:
        void init();
        float getValue();
};

#endif
