#include <Arduino.h>
#include <RotaryAngleSensor.h>

/**
 * Initialise le potentiomètre
 * Définie le port en tant qu'entrée
 **/
void RotaryAngleSensor::init() {
    pinMode(getPort(), INPUT);
}

/**
 * Lit la valeur de l'angle entre 0 et 300°
 */
float RotaryAngleSensor::getValue() {
    return analogRead(getPort()) / 1023 * 300;
}
