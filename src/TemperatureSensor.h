#ifndef TEMPERATURE_SENSOR_H
#define TEMPERATURE_SENSOR_H

#include <Sensor.h>

class TemperatureSensor: public Sensor {

    using Sensor::Sensor;
    
    public:
        void init();
        float getValue();
};

#endif
