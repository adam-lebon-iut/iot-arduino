#include <Arduino.h>
#include <TemperatureSensor.h>

/**
 * Constructeur du capteur de température:
 * Configure le port en mode lecture
 */
void TemperatureSensor::init() {
    pinMode(getPort(), INPUT);
}

/**
 * Récupère la valeur du capteur de température
 */
float TemperatureSensor::getValue() {
    float valTemp = analogRead(getPort());
    float voltage = (valTemp/1024.0) *  5.0;
    return (voltage - .5) * 100;
    
}
