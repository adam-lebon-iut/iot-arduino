#ifndef TEMPERATURE_REGULATOR_H
#define TEMPERATURE_REGULATOR_H

#include <TemperatureSensor.h>
#include <RotaryAngleSensor.h>
#include <LCDEffector.h>

class TemperatureRegulator {
    private:
        TemperatureSensor& m_tempSensor;
        RotaryAngleSensor& m_rotarySensor;
        LCDEffector& m_lcd;

    public:
        TemperatureRegulator(
            TemperatureSensor& tempSensor,
            RotaryAngleSensor& rotarySensor,
            LCDEffector& lcd
        );

        void doObservations();
};

#endif
