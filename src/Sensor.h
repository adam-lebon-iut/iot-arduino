#ifndef SENSOR_H
#define SENSOR_H

#include <stdlib.h>

class Sensor {  
    private:
        int m_port;

    public:
        Sensor(int port);
        int getPort();
        virtual float getValue();

        virtual void init() {}
};

#endif
