#include <TemperatureRegulator.h>
#include <Arduino.h>

/**
 * Constructeur du Regulateur
 * Stocke les instances des sensors/effectors
 * dans les attributs de l'objet
 **/
TemperatureRegulator::TemperatureRegulator(
    TemperatureSensor& tempSensor,
    RotaryAngleSensor& rotarySensor,
    LCDEffector& lcd
):  m_tempSensor(tempSensor),
    m_rotarySensor(rotarySensor),
    m_lcd(lcd) {
};

/**
 * Vérifie les valeurs des capteur,
 * définie si le chauffade doit être allumé
 * et met à jour l'écran si besoin
 **/
void TemperatureRegulator::doObservations() {
    int temperature = m_tempSensor.getValue();
    /**
     * On remap la valeur de l'angle initialement entre 0 et 300°
     * pour avoir une valeur entre 5°C et 30°C
     **/
    int targetTemp = m_rotarySensor.getValue() * 25 / 300 + 5;

    bool heaterState = temperature < targetTemp;

    /**
     * AMÉLIORATION POSSIBLE:
     *      mettre à jour l'affichage uniquement lorsque
     *      les valeurs de températures on changés
     */

    // Tableau de string: chaque element est une ligne
    String message[] = {
        "Actuelle " + String(temperature),
        "Demandee" + String(targetTemp) + " / " + (heaterState ? "ON" : "OFF")
    };

    m_lcd.print(message, 2);
}
