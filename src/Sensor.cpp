#include "Sensor.h"

using namespace std;

/**
 * Constructeur d'un Capteur
 * Assigne la valeur du port passé en paramètre
 * dans l'attribute m_port
 **/ 
Sensor::Sensor(int port): m_port(port) {

}

/**
 * Getter de l'attribut port
 **/
int Sensor::getPort() {
    return m_port;
}
