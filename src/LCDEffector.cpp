#include <Arduino.h>
#include <LCDEffector.h>
#include <LiquidCrystal.h>

/**
 * Constructeur de l'écran LCD
 * Initialise une instance de la library LiquidCrystal
 * avec la configuration passé en paramètre
 **/
LCDEffector::LCDEffector(int rs, int en, int d4, int d5, int d6, int d7) {
    m_lcd = new LiquidCrystal(rs, en, d4, d5, d6, d7);
}

/**
 * Initialise le bus avec l'écran LCD
 **/
void LCDEffector::init() {
    m_lcd->begin(16, 2);
}

/**
 * Affiche du text sur l'écran
 * Chaque élément du tableau correspond à une ligne
 * La taille du tableau doit être passé en second paramètre
 * 
 * AMÉLIORATION POSSIBLE:
 *      Lire la taille du tableau dynamiquement
 *      en utilisant un vector
 **/
void LCDEffector::print(String message[], int length) {
    m_lcd->clear();
    for (int i = 0 ; i < length ; i++) {
        m_lcd->setCursor(0, i);
        m_lcd->print(message[i]);
    }
}
